# msc-thesis-presentation

This repository contains presentations I have given from my master's thesis. They are:
* Physics days 2021 abstract
* Physics days 2021 poster, which is basically one slide presentation
* Slide presentation used in
    * Tapio Rantala's physics seminar course
    * Diploma seminar

Some links:
* [My master's thesis](https://gitlab.com/tolvanea/msc-thesis/)
* [My earlier presentation on Rantala's seminar course](https://gitlab.com/tolvanea/path_integral_examples)
* [Rantala's seminar course](https://homepages.tuni.fi/tapio.rantala/opetus/files/FYS-1550.Fysiikan.seminaari)
* [Physics days](https://www.jyu.fi/en/congress/physicsdays2021)

Some images (and scripts to generate them) are located in master's thesis repo.
