\begin{frame}
    \frametitle{\textbf{Diamagnetic susceptibility at finite temperatures beyond Born--Oppenheimer approximation}}
    \framesubtitle{\underline{Alpi Tolvanen}\textsuperscript{1}, Juha Tiihonen\textsuperscript{1,2}, and Tapio Rantala\textsuperscript{1}\\
    {\textsuperscript{1}\em Physics, Tampere University},
    %
    {\textsuperscript{2}\em Materials Science and Technology Division, Oak Ridge National Laboratory, Tennessee, USA}
    }
    \vspace{-3mm}
    \makebox[1\textwidth][c]{
    \begin{columns}[T,onlytextwidth]
        \begin{column}{0.23\textwidth}
            \begin{block}{Motivation} \vspace{0.22mm} \parbox{\linewidth}{
                % Nonadiabatic nucleus and finite temperatures are usually challenging topics for conventional quantum simulation methods.
                Born--Oppenheimer approximation and $\SI{0}{\kelvin}$ temperature are common assumptions in many quantum simulation methods. Modern applications, such as nuclear magnetic resonance (NMR), require exact magnetic properties. There are not many existing \textit{ab initio} calculations on diamagnetic susceptibility that take nuclear motion and finite temperatures fully into account. In this work we present unseen calculations on the subject. This work builds on top of our group's previous studies on electric susceptibility~[1].
                %is part of master's thesis, and
                %While presented results have not enough precision for applications such as MRI imaging, they may provide unseen data.
            }\end{block}
            \vspace{-2mm}
            \begin{block}{Method} \vspace{0.22mm} \parbox{\linewidth}{
                We utilize \textbf{path integral Monte Carlo} method~[2], which uses imaginary time propagation to obtain nonrelativistic quantum statistics. The method simulates \textit{exactly}:%\footnotemark[1]
                \begin{itemize}
                    \vspace{-0.7mm} \setlength{\itemsep}{0.3mm} %\scriptsize
                    \item[$\bullet$] finite temperatures,
                    \item[$\bullet$] nonadiabatic nuclei, and
                    \item[$\bullet$] many-body effects. % many-body effects ?
                \end{itemize}
                %\vspace{0.5mm}
                %However, the method is computationally heavy, and it is mainly applicable only for a few electron systems.
            } \end{block}
        \end{column}
        \hspace{1mm}
        \begin{column}{0.23\textwidth}
            \begin{block}{} \vspace{0mm} \parbox{\linewidth}{
                \vspace{-1mm}
                Diamagnetic susceptibilities are obtained with estimator~[3]\\
                \vspace{1mm}
                \makebox[\textwidth][c]{
                $\displaystyle\chi = -\frac{\mu_0 k_B T}{\hbar^2}\ev{{\pa{\color{black}\sum_n^N q_n \ScalA_n}}^2},$
                }
                %\vspace{1mm}
                where $T$ is temperature, $n$ is particle index, $q$ is charge, and $\ScalA$ is surface area of imaginary time path.% and expectation value is evaluated over Monte Carlo samples.

                %Nonadiabatic \textit{All Quantum} nuclei are marked with \textbf{AQ}, and fixed Born--Oppenheimer nuclei are marked with \textbf{BO}.
                Following abbreviations are used
                \begin{itemize}
                    \vspace{-0.7mm} \setlength{\itemsep}{0.3mm} %\scriptsize
                    \item[$\bullet$] \textbf{AQ}: nonadiabatic \textit{All Quantum} nuclei
                    \item[$\bullet$] \textbf{BO}: fixed Born--Oppenheimer nuclei
                \end{itemize}
                \vspace{-0.7mm}
            } \end{block}
            \vspace{-2mm}
            \begin{block}{Results} \vspace{0mm} \parbox{\linewidth}{
                Diamagnetic susceptibilities are presented in figures.
                %Helium atom was also calculated, but it had no temperature dependence or adiabatic effects within error margins.
            } \end{block}
            \vspace{-2mm}
            \begin{block}{References}
                \vspace{-1.5mm}
                \begin{thebibliography}{9}
                    \tiny
                    \bibitem{tiihonen} J. Tiihonen, PhD Thesis, Tampere University, (2019).\vspace{-0.5mm}
                    \bibitem{ceperley} D. M. Ceperley, {\it Rev. Mod. Phys.} {\bf 67}, 279 (1995).\vspace{-0.5mm}
                    \bibitem{pollock} E. L. Pollock and K. J. Runge, {\it J. Chem. Phys.} {\bf 96}, 674 (1992).\vspace{-0.5mm}
%                     \bibitem{kaksi} K. Ruud, J. Vaara, J. Lounila, and T. Helgaker, {\it Chem. Phys. Lett.} {\bf 297}, 467 (1998)
%                     \bibitem{kolme} K. Ruud, P.-O. Åstrand, T. Helgaker, and K. V. Mikkelsen, {\it J. Mol. Struct.} {\bf 388}, 231, (1996).
%                     \bibitem{viisi} J. Tiihonen, I. Kylänpää, and T. T. Rantala, {\it Physical Review A} {\bf 94}, 032515, (2016).
                \end{thebibliography}
            \end{block}

%             \begin{block}{Simulation setup} \vspace{1mm} \parbox{\linewidth}{
%                 Temperatures are calculated at range \SI{100}{\kelvin}--\SI{3000}{\kelvin}. Simulated systems include hydrogen atom $\mathrm{H}$, hydrogen molecule $\mathrm{H}_2$ and helium atom $^4\mathrm{He}$. Extreme nonadiabatic effects are inspected with positronium systems $\mathrm{Ps}$ and $\mathrm{Ps}_2$. Here
%             }\end{block}
            %\vspace{3mm}
%             \begin{figure}
%                 \makebox[\textwidth][c]{\includegraphics[width=1.1\linewidth]{pics/He.pdf}}
%                 \vspace{-6.5mm}
%                 \caption{{\scriptsize $\mathrm{He}$}}
%             \end{figure}
        \end{column}
        \hspace{1mm}
        \begin{column}{0.23\textwidth}
            \vspace{-1.5mm}
            \begin{figure}
                \makebox[\textwidth][c]{\includegraphics[width=1.1\linewidth]{pics/H_H2.pdf}}
                \vspace{-6.0mm}
                \caption{{\scriptsize{}Hydrogen atom $\mathrm{H}$ and hydrogen molecule $\mathrm{H}_2$.}}
                \vspace{-2.0mm}
                \begin{itemize}[leftmargin=3mm]
                    \vspace{-0.7mm} \setlength{\itemsep}{0.3mm} \scriptsize
                    \item[$\bullet$] Nucleus of $\mathrm{H}$ is light enough to show nonadiabtic effects between AQ and BO calculations.
                    \item[$\bullet$] Temperature dependence of $\mathrm{H}_2$ AQ is caused by change of nuclear separation.
                    \item[$\bullet$] $\mathrm{H}_2$ reference values have high variance across different sources.
                \end{itemize}
            \end{figure}

        \end{column}
        \hspace{1mm}
        \begin{column}{0.23\textwidth}
            \vspace{-1.5mm}
            \begin{figure}
                \makebox[\textwidth][c]{\includegraphics[width=1.1\linewidth]{pics/Ps_Ps2_taller.pdf}}
                \vspace{-6.5mm}
                \caption{{\scriptsize{}Positronium systems $\mathrm{Ps}$ and $\mathrm{Ps}_2$.}}
                \vspace{-2.0mm}
                \begin{itemize}[leftmargin=3mm]
                    \vspace{-0.7mm} \setlength{\itemsep}{0.3mm} \scriptsize
                    \item[$\bullet$] $\mathrm{Ps}$ and $\mathrm{Ps}_2$ have susceptibility order of magnitude stronger than their proton system counterparts $\mathrm{H}$ and $\mathrm{H}_2$.
                    \item[$\bullet$] $\mathrm{Ps}$ has strong temperature dependence, and so temperature seems to couple with electronic structure if nucleus is very light.
                    \item[$\bullet$] $\mathrm{Ps}_2$ has higher susceptibility than $2 \mathrm{Ps}$, where $\mathrm{H}_2$ has lower susceptibility than $2 \mathrm{H}$.
                \end{itemize}
            \end{figure}

%             \begin{figure}
%                 \makebox[\textwidth][c]{\includegraphics[width=1.1\linewidth]{pics/H2_varM.pdf}}
%                 \vspace{-6.5mm}
%                 \caption{{\scriptsize{}ASDF\\}}
%             \end{figure}
            %\vspace{-6mm}

        \end{column}
    \end{columns}
    }
\end{frame}

